const config = {
    "timeUpdate": 7200000,
    "port": 8000,
    "db": {
        "name": "database.db",
        "table": "covid",
    }

};

export default config;
