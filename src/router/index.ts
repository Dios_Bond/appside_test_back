import Express, {Request, Response, NextFunction} from "express";
import * as dbfunc from "../controller/db_func";
import * as IFace from '../model/index';

const router = Express.Router();

router
.get("/getalldate",
    async (req: Request, res: Response, next: NextFunction) => {
        res.send(await dbfunc.getAllDate());
    }
)
.get("/getallcountry",
    async (req: Request, res: Response, next: NextFunction) => {
        console.log('getallcountry')
        let arrObj: any = await dbfunc.getAllCountry()
        let resArr = arrObj.map((el: {country: string}) => {
           return (el.country)
        })
        res.send(resArr);
    }
)
.get("/getdata",
    async (req: Request, res: Response, next: NextFunction) => {
        res.setHeader("Access-Control-Allow-Origin", "*")
        res.setHeader("Access-Control-Allow-Headers", "origin, content-type, accept");
        console.log("GetData", req.query)
        //res.status(200).send("ok")
        let {country, dateStart, dateEnd} = req.query;
        //console.log(await dbfunc.getDataByParam(country, dateStart, dateEnd))
        res.status(200).send(await dbfunc.getDataByParam(country, dateStart, dateEnd))
    }
);

export default router;