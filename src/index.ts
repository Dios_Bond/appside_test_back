import Express, {Application, Request, Response} from "express";
import Router from "./router/index";
import GetInfo from "./controller/get_info";
import config from "./config/index";
import bodyParser from "body-parser";
import cors from "cors";

const timeLapse: number = config.timeUpdate;

const app: Application = Express();

app.use (cors());
app.use (Router);
app.use ( (req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "origin, content-type, accept");
    next();
})
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//GetInfo();

setInterval(() => {
    GetInfo();
    },
timeLapse);


app.listen(config.port, () => {
    console.info("App start");
});





