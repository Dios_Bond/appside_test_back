import * as sqlite from 'sqlite3';
import config from '../config/index';
import * as IFace from '../model/index';
const Sqlite = sqlite.verbose();
const dbName: string = config.db.name;
const table: string = config.db.table;

const db: sqlite.Database = new Sqlite.Database(dbName);


function checkCreateTable(): void {
    db.run(`CREATE TABLE IF NOT EXISTS ${table} (id INTEGER PRIMARY KEY AUTOINCREMENT, country TEXT, date DATE, confirmed INTEGER, deaths INTEGER, recovered INTEGER)`);
};

function getDataByParam(country: string, dateStart: string, dateEnd: string) {
    let resProm = new Promise( (res, rej) => {
        db.all(`SELECT country, date, confirmed, deaths, recovered FROM ${table} WHERE country IN (${country}) AND date BETWEEN "${dateStart}" and "${dateEnd}";`, (err, row) => {
            res(row)
        });
    });
    return resProm;
}

function getAllDate() {
    let resProm = new Promise( (res, rej) => {
        db.all(`SELECT DISTINCT date FROM ${table};`, (err, row) => {
            console.log(row)
            res(row)
        });
    });
    return resProm;
}

function getAllCountry() {
    let resProm = new Promise( (res, rej) => {
        db.all(`SELECT DISTINCT country FROM ${table};`, (err, row) => {
            res(row)
        });
    });
    return resProm;
};

async function getByCountryDate (country: string, date: Date) {
       
    let resProm = new Promise( (res, rej) => {
        db.get(`SELECT * FROM ${table} WHERE country="${country}" AND date='${date}';`, (err, row) => {
            res(row == undefined) //** return true if data NOT exists */
            }
        );
    }) 
    //console.log(await resProm);
    return await resProm;
};

async function insertRowsCov(data: IFace.IDataCovid): void {
    const stmp = db.prepare(`INSERT INTO ${table} (country, date, confirmed, deaths, recovered) VALUES (?,?,?,?,?)`);
    let {country, date, confirmed, deaths, recovered} = data;
    if (await getByCountryDate(country, date) == true) {
        console.log("Satrt INSER", country, date, confirmed, deaths, recovered);
        stmp.run(country, date, confirmed, deaths, recovered);
    }
};

export {insertRowsCov, checkCreateTable, getAllCountry, getAllDate, getDataByParam};