interface IDataCovid {
    country: string
    date: Date
    confirmed: number
    deaths: number
    recovered: number
};

interface IReqData {
    country: string | undefined
    dateStart: string | undefined
    dateEnd: string | undefined
}

export {IDataCovid, IReqData};