import fetch, {Headers} from "node-fetch";
import * as dbf from "./db_func";
import * as IFace from '../model/index';
const url: string = "https://pomber.github.io/covid19/timeseries.json";

dbf.checkCreateTable();

async function GetInfo (){
//** Get file with info */
let resBody = await fetch(url);
let result = await resBody.json();

    Object.getOwnPropertyNames(result).forEach((name: string) => {
        result[name].forEach((el: IFace.IDataCovid) => {
            el.country = name;
            dbf.insertRowsCov(el);
        });
    })
}


export default GetInfo